<?php
/**
 * @Entity @Table(name="game")
 **/
class Game
{
	/** @Id @Column(type="integer") **/
	protected $id;
	/** @Column(type="boolean") **/
	protected $init=false;
	
	public function __construct()
	{
		$this->id=1;
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function getInit()
	{
		return $this->init;
	}
	
	public function init()
	{
		$this->init = true;
	}
}
