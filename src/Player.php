<?php
// src/Player.php
if(! function_exists('password_hash')) {
	function password_hash($password,$PASSWORD_DEFAULT=null){
		$salt ='$2a$' . str_pad(8, 2, '0', STR_PAD_LEFT) . '$' .substr(strtr(base64_encode(openssl_random_pseudo_bytes(16)), '+', '.'),0, 22);
		return crypt($password, $salt);
	}
}
if(! function_exists('password_verify')) {
	function password_verify($password,$hash){
		return crypt($password, $hash) == $hash;
	}
}
/**
 * @Entity @Table(name="players")
 **/
class Player
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="string",unique=true) **/
    protected $login;
    /** @Column(type="string") **/
    protected $password;
    /** @Column(type="boolean") **/
    protected $npc = false;
    /** @OneToOne(targetEntity="Fleet", mappedBy="player") **/
    private $fleet;
    /** @OneToOne(targetEntity="Sector", mappedBy="player") **/
    private $sector;
    /** @Column(type="boolean") **/
    protected $gameover = false;
    /** @Column(type="integer") **/
    protected $objectivetype = 0;
    /** @Column(type="string",nullable=true) **/
    protected $email;
    /** @Column(type="integer", nullable=true) **/
    protected $deletiondate;
	
    public function getId()
    {
        return $this->id;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function setLogin($login)
    {
        $this->login = $login;
    }
    
    public function getPassword()
    {
		return $this->password;
	}
	
	public function setPassword($password)
	{
		$this->password = password_hash($password,PASSWORD_DEFAULT);
	}
	
	public function checkPassword($password)
	{
		return password_verify($password,$this->password) == $this->password;
	}
	
	public function getFleet()
	{
		return $this->fleet;
	}
	
	public function setFleet($fleet)
	{
		$this->fleet=$fleet;
	}
	
	public function setSector($sector)
	{
		$this->sector=$sector;
	}
	
	public function getSector()
	{
		return $this->sector;
	}
	
	public function gameOver()
	{
		$this->gameover = true;
	}
	
	public function isGameOver()
	{
		return $this->gameover;
	}
	
	public function restart()
	{
		$this->gameover = false;
	}
	
	public function getObjectiveType()
	{
		return $this->objectivetype;
	}
	
	public function setObjectiveType($type)
	{
		$this->objectivetype=$type;
	}
	
	public function setEmail($email)
	{
		if (filter_var($email,FILTER_VALIDATE_EMAIL))
		{
			$this->email = $email;
		}
	}
	
	public function getEmail()
	{
		return $this->email;
	}
	
	public function deleteAccount()
	{
		$this->deletiondate=time();
	}
	
	public function getDeletionDate()
	{
		return $this->deletiondate;
	}
	
	public function abortDeletion()
	{
		$this->deletiondate=null;
	}
}
