<?php
require_once __DIR__.'/../const.php';
// should extend ShipType
/**
 * @Entity @Table(name="ships")
 **/
class Ship
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="text") **/
    protected $name;
    /** @ManyToOne(targetEntity="ShipType") **/
    private $type;
    /** @Column(type="integer") **/
    protected $hp;
    /** @ManyToOne(targetEntity="Fleet", inversedBy="ships")
     * @var Fleet
     */
    private $fleet;
    /** @Column(type="integer") **/
    protected $passengers;
    /** @Column(type="integer") **/
    protected $staff;
    /** @Column(type="boolean") **/
    protected $FTLdrive=true;
    /** @Column(type="boolean") **/
    protected $surviveSystems=true;
    /** @Column(type="integer") **/
    protected $oxygenLevel=100;
    /** @Column(type="integer") **/
    protected $level=0;
    
    public function __construct($fleet,$type)
    {
		$this->fleet=$fleet;
		$this->type=$type;
		$fleet->addShip($this);
	}
    
    public function getId()
    {
		return $this->id;
	}
	
	public function getFleet()
	{
		return $this->fleet;
	}
	
	public function setName($name)
	{
		$this->name = $name;
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function setHP($hp)
	{
		$this->hp=$hp;
	}
	
	public function getHP()
	{
		return $this->hp;
	}
	
	public function getType()
	{
		return $this->type;
	}
	
	public function setPassengers($passengers)
	{
		if ($passengers > $this->type->getMaxPassengers())
		{
			throw new Exception("Too much passengers for this ship");
		}
		$this->passengers = $passengers;
	}
	
	public function getPassengers()
	{
		return $this->passengers;
	}
	
	public function setStaff($staff)
	{
		if ($staff > $this->type->getQualifiedStaff())
		{
			throw new Exception("Too much staff for this ship");
		}
		$this->staff = $staff;
	}
	
	public function getStaff()
	{
		return $this->staff;
	}
	
	public function takeDamage($damage)
	{
		$defense = $this->type->getDefense($this->level);
		if (!is_null($this->fleet->getAdmiralShip()) && $this->fleet->getAdmiralShip()->getId() == $this->getId() && count($this->fleet->getShips()) > 1)
		{
			$defense += ADMIRAL_SHIP_DEFENSE_BONUS;
		}
		$damage -= $defense;
		if ($damage > 0)
		{
			$this->hp -= $damage;
		}
		if ($this->hp <= 0)
		{
			$this->fleet->removeShip($this);
		}
	}
	
	public function getLevel()
	{
		return $this->level;
	}
	
	public function setLevel($level)
	{
		$this->level = $level;
	}
}
