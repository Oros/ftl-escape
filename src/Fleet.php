<?php
/**
 * @Entity @Table(name="fleets")
 **/
 
 use Doctrine\Common\Collections\ArrayCollection;
 
class Fleet
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="text") **/
    protected $name;
    /** @OneToOne(targetEntity="Player",inversedBy="fleet") **/
    private $player;
    /** @Column(type="integer") **/
    protected $x=0;
    /** @Column(type="integer") **/
    protected $y=0;
    /** @Column(type="integer") **/
    protected $z=0;
    /**
     * @OneToMany(targetEntity="Ship", mappedBy="fleet", indexBy="id")
     * @var Ship[]
     **/
    private $ships;
    /** @Column(type="integer") **/
    protected $qualifiedStaff=0;
    /** @Column(type="integer") **/
    protected $food=500;
    /** @Column(type="integer") **/
    protected $fuel=250;
    /** @Column(type="integer") **/
    protected $medicine=100;
    /** @Column(type="integer") **/
    protected $material=1000;
    /** @Column(type="integer") **/
    protected $moral=40;
    /** @Column(type="integer") **/
    protected $jumpStatus=0;
    /** @OneToMany(targetEntity="HistoryItem", mappedBy="fleet", indexBy="id")
     * @var HistoryItem[]
     **/
    protected $history;
    /** @OneToOne(targetEntity="Ship") nullable=true**/
    private $admiralShip;  
    
    /*
     * Jump status : 
     * 
     * -1 : not functionnal
     * 0 : not ready
     * 1 : set coordinates
     * 2 : transmitting new coordinates
     * 3 : warming FTL drive (up to 25%)
     * 4 : warming FTL drive (up to 50%)
     * 5 : warming FTL drive (up to 75%)
     * 6 : warming FTL drive (up to 100%)
     * 7 : synchronizing with fleet (up to 50%)
     * 8 : synchronizing with fleet (up to 75%)
     * 9 : synchronizing with fleet (up to 100%)
     * 10 : JUMP ! (should not appear or only for transitionnal state)
     */
    
    public function __construct($player)
    {
		$this->ships = new ArrayCollection();
		$this->history = new ArrayCollection();
		$this->setPlayer($player);
	}
    
    public function getId()
    {
		return $this->id;
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function setPlayer($player)
	{
		$this->player=$player;
	}
	
	public function getPlayer()
	{
		return $this->player;
	}
	
	public function setName($name)
	{
		$this->name = $name;
	}
	
	public function getX()
	{
		return $this->x;
	}
	
	public function getY()
	{
		return $this->y;
	}
	
	public function getZ()
	{
		return $this->z;
	}
	
	public function setX($x)
	{
		$this->x=$x;
	}
	
	public function setY($y)
	{
		$this->y=$y;
	}
	
	public function setZ($z)
	{
		$this->z=$z;
	}

	public function getShips()
	{
		return $this->ships->toArray();
	}
	
	public function getShip($id)
	{
		if (!isset($this->ships[$id]))
		{
			echo "not in ships";
		}
		else
		{
			return $this->ships[$id];
		}
	}
	
	public function addShip($ship)
	{
		$this->ships[$ship->getId()] = $ship;
	}
	
	public function removeShip($ship)
	{
		
	}
	
	public function getAdmiralShip()
	{
		return $this->admiralShip;
	}
	
	public function setAdmiralShip($ship)
	{
		$this->admiralShip = $ship;
	}
	
	public function isEmpty()
	{
		$compo = $this->ships;
		return is_null($compo) || empty($compo) || count($compo) == 0;
	}
	
	public function setJumpStatus($status)
	{
		$this->jumpStatus=$status;
	}
	
	public function getJumpStatus()
	{
		return $this->jumpStatus;
	}
	
	public function getCombinedAttack()
	{
		$value = 0;
		foreach ($this->ships as $ship)
		{
			$level = $ship->getLevel();
			$value += $ship->getType()->getAttack($level);
		}
		return $value;
	}
	
	public function getMaterial()
	{
		return $this->material;
	}
	
	public function setMaterial($material)
	{
		$this->material=$material;
	}
	
	public function increaseMaterial($ammount)
	{
		$this->material += $ammount;
	}
	
	public function decreaseMaterial($ammount)
	{
		if ($ammount > $this->material)
		{
			throw new Exception("Not enough materials in fleet");
		}
		$this->material -= $ammount;
	}
	
	public function getFood()
	{
		return $this->food;
	}
	
	public function setFood($food)
	{
		$this->food = $food;
	}
	
	public function getFuel()
	{
		return $this->fuel;
	}
	
	public function setFuel($fuel)
	{
		$this->fuel = $fuel;
	}
	
	public function decreaseFuel($ammount)
	{
		if ($ammount > $this->fuel)
		{
			throw new Exception("Not enough fuel in fleet");
		}
		$this->fuel -= $ammount;
	}
	
	public function increaseFuel($ammount)
	{
		$this->fuel += $ammount;
	}
	
	public function getMedicine()
	{
		return $this->medicine;
	}
	
	public function setMedicine($med)
	{
		$this->medicine = $med;
	}
	
	public function increaseMedicine($ammount)
	{
		$this->medicine += $ammount;
	}
	
	public function decreaseMedicine($ammount)
	{
		if ($ammount > $this->medicine)
		{
			throw new Exception("Not enough medicine in the fleet");
		}
		$this->medicine -= $ammount;
	}
	
	public function getMoral()
	{
		return $this->moral;
	}
	
	public function setMoral($moral)
	{
		$this->moral = $moral;
	}
	
	public function decreaseMoral($ammount)
	{
		if ($ammount > $this->moral)
		{
			$ammount = $this->moral;
		}
		$this->moral -= $ammount;
	}
	
	public function increaseMoral($ammount)
	{
		$this->moral += $ammount;
		if ($this->moral > 100)
		{
			$this->moral = 100;
		}
	}
	
	public function getHistory()
	{
		return $this->history;
	}
	
	public function addShipToHistory($ship)
	{
		if (is_null($this->history))
		{
			$this->history = new ArrayCollection();
		}
		$histShip = new HistoryItem($ship);
		$this->history[$histShip->getId()] = $histShip;
	}
	
	public function getQualifiedStaff()
	{
		return $this->qualifiedStaff;
	}
	
	public function increaseQualifiedStaff($ammount)
	{
		$this->qualifiedStaff += $ammount;
	}
	
	public function decreaseQualifiedStaff($ammount)
	{
		if ($ammount > $this->qualifiedStaff)
		{
			throw new Exception("Not enough qualified staff");
		}
		$this->qualifiedStaff -= $ammount;
	}
}
