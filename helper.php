<?php

require_once(__DIR__.'/const.php');

class Helper
{
	static function canScout($fleet)
	{
		$ships = $fleet->getShips();
		$canScout = false;
		foreach($ships as $ship)
		{
			$canScout = $canScout || $ship->getType()->canScout();
		}
		return $canScout;
	}
	
	static function calculateDifficulty($player)
	{
		$fleet = $player->getFleet();
		$attack = $fleet->getCombinedAttack();
		$ships = $fleet->getShips();
		$defense = 0;
		foreach($ships as $ship)
		{
			$defense += $ship->getType()->getDefense();
		}
		$difficulty = round(($attack * DIFFICULTY_CALC_MULTIPLIER_ATTACK + $defense * DIFFICULTY_CALC_MULTIPLIER_DEFENSE) / DIFFICULTY_CALC_DIVIDER,0);
		if ($difficulty > MAX_DIFFICULTY)
		{
			$difficulty = MAX_DIFFICULTY;
		}
		return $difficulty;
	}
	
	static function canProduce($fleet)
	{
		$ships = $fleet->getShips();
		$canProduce = false;
		while (!$canProduce && list(,$ship) = each($ships))
		{
			$canProduce = $canProduce || $ship->getType()->canProduceShips();
		}
		return $canProduce;
	}
	
	static function canAct($player)
	{
		return !Helper::underAttack($player);
	}
	
	static function underAttack($player)
	{
		$sector = $player->getSector();
		return count($sector->getEnnemies()) > 0;
	}
	
	static function calculateSurvivors($player)
	{
		$survivors = 0;
		$ships = $player->getFleet()->getShips();
		foreach ($ships as $ship)
		{
			$survivors += $ship->getPassengers() + $ship->getStaff();
		}
		return $survivors;
	}
}
