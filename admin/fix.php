<?php

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../lib/i18n.php');

$ships = $entityManager->getRepository('Ship')->findAll();

foreach ($ships as $ship)
{
	if ($ship->getLevel() > SHIP_UPGRADE_MAX_LEVEL)
	{
		$l = $ship->getLevel();
		$diff = $ship->getLevel() - SHIP_UPGRADE_MAX_LEVEL;
		$ship->setLevel(SHIP_UPGRADE_MAX_LEVEL);
		$fleet = $ship->getFleet();
		$rembours = round($diff * ($ship->getType()->getPrice() * SHIP_UPGRADE_PRICE));
		echo 'Réembourse '.$rembours.' à '.$fleet->getPlayer()->getLogin().' ('.$ship->getType()->getName().'/lvl '.$l.')<br />';
		$fleet->increaseMaterial($rembours);
		$player = $fleet->getPlayer();
		$message = new Message(null,$player,'Le vaisseau '.$ship->getName().' avait un niveau supérieur à Mk.12. Vous avez été remboursé de '.$rembours.' matériaux.',true);
		$entityManager->persist($message);
	}
}
$entityManager->flush();
