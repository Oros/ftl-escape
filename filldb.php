<?php
require_once __DIR__."/bootstrap.php";
require_once __DIR__."/tools.php";
require_once __DIR__."/const.php";

$game = $entityManager->getRepository('Game')->find(1);

if (is_null($game))
{
	
	$game = new Game();
	$game->init();
	$entityManager->persist($game);

// SHIP TYPES

$battlecruiser = new ShipType();
$battlecruiser->setName('ship.battlecruiser.name');
$battlecruiser->setAttack(200);
$battlecruiser->setDefense(500);
$battlecruiser->setMaxHP(2000);
$battlecruiser->setMaxPassengers(10000);
$battlecruiser->setQualifiedStaff(5000);
$battlecruiser->setFoodProduction(1);
$battlecruiser->setFuelProduction(1);
$battlecruiser->setMaterialProduction(1);
$battlecruiser->setMedicineProduction(1);
$battlecruiser->setMoralProduction(2);
$battlecruiser->setQualifiedStaffPerCycle(0);
$battlecruiser->setPrice(340000);
$battlecruiser->setNameDico('military');

$entityManager->persist($battlecruiser);

$transport = new ShipType();
$transport->setName('ship.transport.name');
$transport->setAttack(0);
$transport->setDefense(5);
$transport->setMaxHP(50);
$transport->setMaxPassengers(2000);
$transport->setQualifiedStaff(50);
$transport->setFoodProduction(0);
$transport->setFuelProduction(0);
$transport->setMaterialProduction(0);
$transport->setMedicineProduction(0);
$transport->setMoralProduction(0.1);
$transport->setQualifiedStaffPerCycle(1);
$transport->setPrice(50000);
$transport->setNameDico('civilians');

$entityManager->persist($transport);

$miningbarge = new ShipType();
$miningbarge->setName('ship.mining.barge.name');
$miningbarge->setAttack(0);
$miningbarge->setDefense(10);
$miningbarge->setMaxHP(200);
$miningbarge->setMaxPassengers(500);
$miningbarge->setQualifiedStaff(1000);
$miningbarge->setFoodProduction(0);
$miningbarge->setFuelProduction(0.1);
$miningbarge->setMaterialProduction(50);
$miningbarge->setMedicineProduction(0);
$miningbarge->setMoralProduction(0.1);
$miningbarge->setQualifiedStaffPerCycle(0);
$miningbarge->enableMining();
$miningbarge->setPrice(95000);
$miningbarge->setNameDico('civilians');

$entityManager->persist($miningbarge);

$ships = array(	array('name'=>'ship.refinery.name','attack'=>0,'defense'=>10,'hp'=>230,'passengers'=>10,'staff'=>750,'food'=>0,'fuel'=>3,'material'=>0,'medicine'=>0,'moral'=>0.1,'qstaffcycle'=>0,'special'=>'','price'=>270000,'dico'=>'civilians'),
				array('name'=>'ship.pleasure.boat.name','attack'=>0,'defense'=>2,'hp'=>100,'passengers'=>750,'staff'=>400,'food'=>0,'fuel'=>0,'material'=>0,'medicine'=>0,'moral'=>1,'qstaffcycle'=>0,'special'=>'','price'=>125000,'dico'=>'civilians'),
				array('name'=>'ship.research.lab.name','attack'=>0,'defense'=>10,'hp'=>50,'passengers'=>20,'staff'=>40,'food'=>0,'fuel'=>0,'material'=>0,'medicine'=>0.5,'moral'=>0.2,'qstaffcycle'=>1,'special'=>'','price'=>35000,'dico'=>'civilians'),
				array('name'=>'ship.battle.hospital.name','attack'=>0,'defense'=>150,'hp'=>300,'passengers'=>150,'staff'=>300,'food'=>0,'fuel'=>0,'material'=>0,'medicine'=>0.5,'moral'=>1,'qstaffcycle'=>1,'special'=>'','price'=>165000,'dico'=>'military'),
				array('name'=>'ship.frigate.name','attack'=>100,'defense'=>150,'hp'=>750,'passengers'=>100,'staff'=>500,'food'=>0,'fuel'=>0,'material'=>0,'medicine'=>0,'moral'=>1,'qstaffcycle'=>0,'special'=>'','price'=>180000,'dico'=>'military'),
				array('name'=>'ship.fighters.squad.name','attack'=>2,'defense'=>2,'hp'=>5,'passengers'=>0,'staff'=>5,'food'=>0,'fuel'=>0,'material'=>0,'medicine'=>0,'moral'=>0.5,'qstaffcycle'=>0,'special'=>'','price'=>3000,'dico'=>'fighters'),
				array('name'=>'ship.scout.name','attack'=>0,'defense'=>100,'hp'=>50,'passengers'=>0,'staff'=>30,'food'=>0,'fuel'=>0,'material'=>0,'medicine'=>0,'moral'=>0.2,'qstaffcycle'=>0,'special'=>'scout','price'=>3600,'dico'=>'military'),
				array('name'=>'ship.factory.name','attack'=>0,'defense'=>50,'hp'=>300,'passengers'=>1000,'staff'=>850,'food'=>0,'fuel'=>0,'material'=>0,'medicine'=>0,'moral'=>0.1,'qstaffcycle'=>0,'special'=>'produce','price'=>70000,'dico'=>'civilians'),
				array('name'=>'ship.repair.name','attack'=>0,'defense'=>60,'hp'=>200,'passengers'=>500,'staff'=>1200,'food'=>0,'fuel'=>0,'material'=>0,'medicine'=>0,'moral'=>0.1,'qstaffcycle'=>0,'special'=>'repair','price'=>23000,'dico'=>'civilians'),
				array('name'=>'ship.exploitation.name','attack'=>0,'defense'=>10,'hp'=>200,'passengers'=>300,'staff'=>120,'food'=>0,'fuel'=>0.5,'material'=>2,'medicine'=>0,'moral'=>0.1,'qstaffcycle'=>1,'special'=>'mining','price'=>14000,'dico'=>'civilians'),
				array('name'=>'ship.farm.name','attack'=>0,'defense'=>50,'hp'=>150,'passengers'=>800,'staff'=>80,'food'=>1,'fuel'=>0,'material'=>0,'medicine'=>0,'moral'=>0.2,'qstaffcycle'=>0,'special'=>'','price'=>8000,'dico'=>'civilians'),
				array('name'=>'ship.corvette.name','attack'=>50,'defense'=>40,'hp'=>200,'passengers'=>50,'staff'=>100,'food'=>0,'fuel'=>0,'material'=>0,'medicine'=>0,'moral'=>0.1,'qstaffcycle'=>0,'special'=>'','price'=>60000,'dico'=>'military')
				);
				
foreach($ships as $ship)
{
	$shipType = new ShipType();
	$shipType->setName($ship['name']);
	$shipType->setAttack($ship['attack']);
	$shipType->setDefense($ship['defense']);
	$shipType->setMaxHP($ship['hp']);
	$shipType->setMaxPassengers($ship['passengers']);
	$shipType->setQualifiedStaff($ship['staff']);
	$shipType->setFoodProduction($ship['food']);
	$shipType->setFuelProduction($ship['fuel']);
	$shipType->setMaterialProduction($ship['material']);
	$shipType->setMedicineProduction($ship['medicine']);
	$shipType->setMoralProduction($ship['moral']);
	$shipType->setQualifiedStaffPerCycle($ship['qstaffcycle']);
	$shipType->setPrice($ship['price']);
	$shipType->setNameDico($ship['dico']);
	
	if ($ship['special'] == 'scout')
	{
		$shipType->enableScout();
	}
	if ($ship['special'] == 'produce')
	{
		$shipType->enableProduceShips();
	}
	if ($ship['special'] == 'repair')
	{
		$shipType->enableRepair();
	}
	if ($ship['special'] == 'mining')
	{
		$shipType->enableMining();
	}
	
	$entityManager->persist($shipType);
}

$entityManager->flush();

// ENNEMY SHIP TYPES

$ennemyships = array(	array('name'=>'ship.ennemy.cruiser.name','attack'=>100,'defense'=>100,'hp'=>300,'difficulty'=>2),
						array('name'=>'ship.ennemy.frigate.name','attack'=>60,'defense'=>60,'hp'=>100,'difficulty'=>1),
						array('name'=>'ship.ennemy.battlecruiser.name','attack'=>200,'defense'=>180,'hp'=>800,'difficulty'=>3),
						array('name'=>'ship.ennemy.light.frigate.name','attack'=>30,'defense'=>30,'hp'=>50,'difficulty'=>1),
						array('name'=>'ship.ennemy.heavy.fighter.squad.name','attack'=>20,'defense'=>10,'hp'=>20,'difficulty'=>1),
						array('name'=>'ship.ennemy.fighter.squad.name','attack'=>10,'defense'=>10,'hp'=>10,'difficulty'=>1),
						array('name'=>'ship.ennemy.mothership.name','attack'=>600,'defense'=>350,'hp'=>3000,'difficulty'=>4),
						array('name'=>'ship.ennemy.destroyer','attack'=>80,'defense'=>110,'hp'=>200,'difficulty'=>2),
						array('name'=>'ship.ennemy.patrol','attack'=>25,'defense'=>20,'hp'=>30,'difficulty'=>1),
						array('name'=>'ship.ennemy.ironclad','attack'=>250,'defense'=>200,'hp'=>1000,'difficulty'=>3)
					);

foreach ($ennemyships as $ship)
{
	$type = new EnnemyShipType();
	$type->setName($ship['name']);
	$type->setAttack($ship['attack']);
	$type->setDefense($ship['defense']);
	$type->setMaxHP($ship['hp']);
	$type->setDifficulty($ship['difficulty']);

	$entityManager->persist($type);
}
$entityManager->flush();

/*
// PLANETARY EQUIPMENTS TYPES

$commcenter = new PlanetaryEquipmentType();
$commcenter->setId(COMMUNICATION_CENTER_ID);
$commcenter->setImgLabel("commcenter");
$commcenter->setName("label.equipment.name.communication_center");
$commcenter->setUnique(true);

$entityManager->persist($commcenter);

$equipmentType = new PlanetaryEquipmentType();
$equipmentType->setId(FACTORY_ID);
$equipmentType->setImgLabel("factory");
$equipmentType->setName("label.equipment.name.factory");
$factory = $equipmentType;

$entityManager->persist($equipmentType);

$equipmentType = new PlanetaryEquipmentType();
$equipmentType->setId(LAB_ID);
$equipmentType->setImgLabel("lab");
$equipmentType->setName("label.equipment.name.lab");
$lab = $equipmentType;

$entityManager->persist($equipmentType);

$equipmentType = new PlanetaryEquipmentType();
$equipmentType->setId(FARM_ID);
$equipmentType->setImgLabel("farm");
$equipmentType->setName("label.equipment.name.farm");

$entityManager->persist($equipmentType);

$equipmentType = new PlanetaryEquipmentType();
$equipmentType->setId(FORT_ID);
$equipmentType->setImgLabel("fort");
$equipmentType->setName("label.equipment.name.fort");
$equipmentType->setRequiredResearchLevel(2);

$entityManager->persist($equipmentType);

$equipmentType = new PlanetaryEquipmentType();
$equipmentType->setId(SILO_ID);
$equipmentType->setImgLabel("silo");
$equipmentType->setName("label.equipment.name.silo");
$equipmentType->setRequiredResearchLevel(5);

$entityManager->persist($equipmentType);

$equipmentType = new PlanetaryEquipmentType();
$equipmentType->setId(MAINTENANCE_ID);
$equipmentType->setImgLabel("maintenance");
$equipmentType->setName("label.equipment.name.maintenance");
$equipmentType->setRequiredResearchLevel(0);

$entityManager->persist($equipmentType);

$entityManager->flush();
// SHIPS

$fighter = new ShipType();
$fighter->setName("ship.fighter.name");
$fighter->setHP(1);
$fighter->setDefense(2);
$fighter->setResearchLvl(0);
$fighter->setAttack(1);
$fighter->setProductionCost(5);

$entityManager->persist($fighter);
$fighter_id = $fighter->getId();

$bomber = new ShipType();
$bomber->setName("ship.bomber.name");
$bomber->setHP(2);
$bomber->setDefense(1);
$bomber->setResearchLvl(1);
$bomber->setAttack(2);
$bomber->setProductionCost(6);
$bomber->setMaintenanceCost(2);
$bomber->setBombingValue(1);

$entityManager->persist($bomber);

$frigate = new ShipType();
$frigate->setName("ship.frigate.name");
$frigate->setHP(10);
$frigate->setDefense(3);
$frigate->setResearchLvl(2);
$frigate->setAttack(5);
$frigate->setProductionCost(10);
$frigate->setMaintenanceCost(3);
$frigate->setStrongAgainst($fighter);

$entityManager->persist($frigate);

$ship = new ShipType();
$ship->setName("ship.cargo.name");
$ship->setHP(20);
$ship->setDefense(0);
$ship->setResearchLvl(2);
$ship->setAttack(0);
$ship->setMaintenanceCost(4);
$ship->setProductionCost(20);
$ship->toggleCarryArtefact();

$entityManager->persist($ship);

$ship = new ShipType();
$ship->setName("ship.destroyer.name");
$ship->setHP(25);
$ship->setDefense(4);
$ship->setResearchLvl(2);
$ship->setAttack(6);
$ship->setMaintenanceCost(3);
$ship->setProductionCost(12);
//$ship->setStrongAgainst($fighter);

$entityManager->persist($ship);

$ship = new ShipType();
$ship->setName("ship.cruiser.name");
$ship->setHP(55);
$ship->setDefense(6);
$ship->setResearchLvl(3);
$ship->setAttack(15);
$ship->setMaintenanceCost(5);
$ship->setProductionCost(25);

$entityManager->persist($ship);
$cruiser_id = $ship->getId();

$ship = new ShipType();
$ship->setName("ship.heavy.cruiser.name");
$ship->setHP(65);
$ship->setDefense(7);
$ship->setResearchLvl(3);
$ship->setAttack(17);
$ship->setMaintenanceCost(7);
$ship->setProductionCost(32);

$entityManager->persist($ship);

$ship = new ShipType();
$ship->setName("ship.dreadnought.name");
$ship->setHP(80);
$ship->setDefense(15);
$ship->setResearchLvl(4);
$ship->setAttack(30);
$ship->setMaintenanceCost(10);
$ship->setProductionCost(50);
$ship->setBombingValue(5);

$entityManager->persist($ship);

$ship = new ShipType();
$ship->setName("ship.battleship.name");
$ship->setHP(100);
$ship->setDefense(25);
$ship->setResearchLvl(5);
$ship->setAttack(50);
$ship->setMaintenanceCost(15);
$ship->setProductionCost(110);
$ship->setBombingValue(7);

$entityManager->persist($ship);
$battleship_id = $ship->getId();

$ship = new ShipType();
$ship->setName("ship.battlecruiser.name");
$ship->setHP(150);
$ship->setDefense(40);
$ship->setResearchLvl(6);
$ship->setAttack(65);
$ship->setMaintenanceCost(20);
$ship->setProductionCost(155);
$ship->setBombingValue(10);
$battlecruiser = $ship;

$entityManager->persist($ship);
$battlecruiser_id = $ship->getId();

$ship = new ShipType();
$ship->setName("ship.carrier.name");
$ship->setHP(100);
$ship->setDefense(5);
$ship->setResearchLvl(6);
$ship->setAttack(0);
$ship->setProductionCost(200);
$ship->setMaintenanceCost(30);
$ship->toggleCarryArtefact();

$entityManager->persist($ship);

$ship = new ShipType();
$ship->setName("ship.titan.name");
$ship->setHP(500);
$ship->setDefense(150);
$ship->setResearchLvl(7);
$ship->setAttack(200);
$ship->setMaintenanceCost(50);
$ship->setProductionCost(500);
$ship->setBombingValue(50);
$ship->toggleCarryArtefact();

$entityManager->persist($ship);

$entityManager->flush();
$titan_id = $ship->getId();
// NEUTRAL PLAYER

$player = new Player();
$player->setLogin("Neutre");
$player->setPassword("testouille");
$player->setAsNPC();

$entityManager->persist($player);

$ship = $entityManager->find('ShipType',$titan_id);
$planetsArr = array(
					array('name'=>'New Earth'),
					array('name'=>'Alpha Centauri B'),
					array('name'=>'Verda'),
					array('name'=>'CVMB-3'),
					array('name'=>'Sheppard'),
					array('name'=>'Acheron'),
					array('name'=>'Antarès'),
					array('name'=>'Solaria'),
					array('name'=>'Caladan'),
					array('name'=>'Io Prime'),
					array('name'=>'Limbo'),
					array('name'=>'Vulcain'),
					array('name'=>'Xeno 4'),
					array('name'=>'Deroba'),
					array('name'=>'Techorian')
					);

foreach ($planetsArr as $plan)
{
	$planet = new Planet();
	$planet->setName($plan['name']);
	$planet->setOwner($player);
	$coordinates = Tools::getFreeRandomCoordinates();
	$planet->setX($coordinates['x']);
	$planet->setY($coordinates['y']);
	$planet->setZ($coordinates['z']);
	$planet->addGarrison($ship,5);

	$entityManager->persist($planet);

	$equipment = new PlanetaryEquipment();
	$equipment->setType($commcenter);
	$equipment->setX(0);
	$equipment->setY(0);
	$equipment->setZ(0);
	$equipment->setPlanet($planet);

	$entityManager->persist($equipment);
}

// FEDERATION PLAYER

$player = new Player();
$player->setLogin("Federation");
$player->setPassword("testouille");
$player->setAsNPC();

$entityManager->persist($player);

$ship = $entityManager->find('ShipType',$titan_id);
$planetsArr = array(
					array('name'=>'Caprica'),
					array('name'=>'Aerilon'),
					array('name'=>'Aquaria'),
					array('name'=>'Canceron'),
					array('name'=>'Gemenon'),
					array('name'=>'Leonis'),
					array('name'=>'Libris'),
					array('name'=>'Sagittaron'),
					array('name'=>'Scorpia'),
					array('name'=>'Tauron'),
					array('name'=>'Virgon'),
					array('name'=>'Picon')
					);

foreach ($planetsArr as $plan)
{
	$planet = new Planet();
	$planet->setName($plan['name']);
	$planet->setOwner($player);
	$coordinates = Tools::getFreeRandomCoordinates();
	$planet->setX($coordinates['x']);
	$planet->setY($coordinates['y']);
	$planet->setZ($coordinates['z']);
	$planet->addGarrison($ship,5);

	$entityManager->persist($planet);

	$equipment = new PlanetaryEquipment();
	$equipment->setType($commcenter);
	$equipment->setX(0);
	$equipment->setY(0);
	$equipment->setZ(0);
	$equipment->setPlanet($planet);

	$entityManager->persist($equipment);
}

// TEST USER

$player = new Player();
$player->setLogin("test1");
$player->setPassword("testouille");
$player->setResearchLvl(0);

$entityManager->persist($player);

$planet = new Planet();
$planet->setName("Testor Planet");
$planet->setOwner($player);
$coordinates = Tools::getFreeRandomCoordinates();
$planet->setX($coordinates['x']);
$planet->setY($coordinates['y']);
$planet->setZ($coordinates['z']);

$entityManager->persist($planet);

$equipment = new PlanetaryEquipment();
$equipment->setType($commcenter);
$equipment->setX(0);
$equipment->setY(0);
$equipment->setZ(0);
$equipment->setPlanet($planet);

$entityManager->persist($equipment);

$equipment = new PlanetaryEquipment();
$equipment->setType($factory);
$equipment->setX(1);
$equipment->setY(1);
$equipment->setZ(0);
$equipment->setPlanet($planet);

$entityManager->persist($equipment);

$equipment = new PlanetaryEquipment();
$equipment->setType($lab);
$equipment->setX(0);
$equipment->setY(1);
$equipment->setZ(0);
$equipment->setPlanet($planet);

$entityManager->persist($equipment);

// TEST USER 2

$player = new Player();
$player->setLogin("test2");
$player->setPassword("testouille");
$player->setResearchLvl(0);

$entityManager->persist($player);

$planet = new Planet();
$planet->setName("Testor 2");
$planet->setOwner($player);
$coordinates = Tools::getFreeRandomCoordinates();
$planet->setX($coordinates['x']);
$planet->setY($coordinates['y']);
$planet->setZ($coordinates['z']);

$entityManager->persist($planet);

$equipment = new PlanetaryEquipment();
$equipment->setType($commcenter);
$equipment->setX(0);
$equipment->setY(0);
$equipment->setZ(0);
$equipment->setPlanet($planet);

$entityManager->persist($equipment);

$equipment = new PlanetaryEquipment();
$equipment->setType($factory);
$equipment->setX(1);
$equipment->setY(1);
$equipment->setZ(0);
$equipment->setPlanet($planet);

$entityManager->persist($equipment);

$equipment = new PlanetaryEquipment();
$equipment->setType($lab);
$equipment->setX(0);
$equipment->setY(1);
$equipment->setZ(0);
$equipment->setPlanet($planet);

$entityManager->persist($equipment);*/

$entityManager->flush();
}
else
{
	echo "Game already set";
}
