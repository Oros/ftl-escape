<h1>{$lbl_galaxy}</h1>
<div class="datagrid">
<table id="tbl_galaxy">
<thead><tr><th>{$lbl_player}</th><th>{$lbl_nb_ships}</th><th>{$lbl_nb_survivors}</th></tr></thead>
<tbody>
	{$nb_ships=0}
	{$nb_survivors=0}
	{foreach $players as $player}
	{$nbs = count($player->getFleet()->getShips())}
	{$nbv = Helper::calculateSurvivors($player)}
	{$nb_ships = $nb_ships + $nbs}
	{$nb_survivors = $nb_survivors + $nbv}
	<tr><td>{$player->getLogin()}</td><td>{$nbs}</td><td>{$nbv}</td></tr>
	{/foreach}
</tbody>
<tfoot><tr><td></td><td>{$nb_ships}</td><td>{$nb_survivors}</td></tr></tfoot>
</table>
</div>
<script type="text/javascript">
sortable_table("tbl_galaxy");
</script>
