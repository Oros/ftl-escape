<div id="fleet">
	<h1>{$fleetname} <a href="?page=editfleetname"><img src="img/edit.png" alt="Edit"/></a>({$nb_ships} {$w_ships})</h1>
	<div id="status">
		<table>
		<tr>
			<td class="power_{if $can_repair}green{else}red{/if}">{$lbl_power_repair}</td>
			<td class="power_{if $can_mine}green{else}red{/if}">{$lbl_power_mine}</td>
			<td class="power_{if $can_scout}green{else}red{/if}">{$lbl_power_scout}</td>
			<td class="power_{if $can_produce}green{else}red{/if}">{$lbl_power_produce}</td>
		</tr>
		</table>
	</div>
	<div id="summary" class="datagrid">
		<table id="tbl_fleet">
			<thead><tr><th>{$lbl_ship_name}</th><th>{$lbl_ship_type_name}</th><th>{$lbl_ship_hp}</th><th>{$lbl_ship_passengers}</th><th>{$lbl_ship_staff}</th><th>{$lbl_ship_attack}</th><th>{$lbl_ship_defense}</th></tr></thead>
			<tbody>
			{$repairMaterials=0}
			{foreach $ships as $ship}
			{$enoughStaff = $ship->getStaff() > 0}
			{$alert = !$enoughStaff}
			{$is_admiral = $has_admiral_ship && $admiral_ship->getId() == $ship->getId()}
				<tr {if $alert}class="alert"{/if}>
					<td>{$ship->getName()}<a class="name_edit_link" href="?page=editshipname&amp;id={$ship->getId()}"><img src="img/edit.png" alt="Edit"/></a>{if $is_admiral}<img id="admiral_badge" src="img/admiral_ship.png" alt="Admiral"/>{else}<a href="makeadmiral.php?id={$ship->getId()}" class="admiral_edit_link"><img src="img/admiral_link.png" alt="Edit link"/></a>{/if}{if $can_act && $ship->getLevel() < $max_upgrade_level}<a href="upgradeship.php?id={$ship->getId()}" class="tooltip upgrade_link"><img src="img/upgrade.png"  alt="Upgrade"/><span class="classic">{round($ship->getType()->getPrice() * $upgrade_ratio + $ship->getType()->getMaxHP($ship->getLevel()) - $ship->getHP())} {$i18n->getText('lbl.materials')}</span></a>{/if}{if $alert}{if !$enoughStaff}<a class="tooltip alert_staff"><img src="img/alert_staff.png" alt="Alert staff"/><span class="classic">{$msg_not_enough_staff}</span></a>{/if}{/if}</td>
					<td>{$i18n->getText($ship->getType()->getName())} (Mk. {$ship->getLevel() + 1})</td>
					<td {if $ship->getHP() < $ship->getType()->getMaxHP($ship->getLevel())}class="important"{/if} >{$ship->getHP()}/{$ship->getType()->getMaxHP($ship->getLevel())} {if $ship->getHP() < $ship->getType()->getMaxHP($ship->getLevel()) && $can_repair && $can_act}{$repairMaterials = $repairMaterials + ($ship->getType()->getMaxHP($ship->getLevel()) - $ship->getHP())} <a href="repair.php?id={$ship->getId()}">{$lbl_repair}</a>{/if}</td>
					<td>{$ship->getPassengers()}/{$ship->getType()->getMaxPassengers()}</td>
					<td {if !$enoughStaff}class="important"{/if}>{$ship->getStaff()}/{$ship->getType()->getQualifiedStaff()} {if $fleet->getQualifiedStaff() > 0 && $ship->getStaff() < $ship->getType()->getQualifiedStaff()}<a href="assignstaff.php?id={$ship->getId()}">{$i18n->getText('lbl.assign.staff')}</a>{/if}</td>
					<td>{round($ship->getType()->getAttack() * pow($ship_level_multiplier,$ship->getLevel()))}</td>
					<td>{if $is_admiral && $nb_ships > 1}{round($ship->getType()->getDefense() * pow($ship_level_multiplier,$ship->getLevel())) + $admiral_defense_bonus}{else}{round($ship->getType()->getDefense() * pow($ship_level_multiplier,$ship->getLevel()))}{/if}</td>
				</tr>
			{/foreach}
			</tbody>
			<tfoot>
			<tr><td>Total</td><td></td><td>{if $repairMaterials > 0}<a href="repairall.php" class="tooltip" id="fleet_repair_all">{$i18n->getText('lbl.repair.all')}<span class="classic">{$repairMaterials} {$i18n->getText('lbl.materials')}</span></a>{/if}</td><td colspan=2>{$survivors}{if $can_act} (<a href="?page=transfer">{$lbl_transfer}</a>){/if}</td><td>{$combined_attack}</td><td>{$combined_defense}</td></tr>
			</tfoot>
		</table>
	</div>
	<div class="clear"></div>
	<div id="stock" class="datagrid">
		<table>
			<thead><tr><th></th><th>{$fleet_th_stock}</th><th>{$fleet_th_conso}</th><th>{$fleet_th_prod}</th></tr></thead>
			<tbody>
			<tr><td>Food</td><td>{$fleet->getFood()}</td><td>{$food_conso}/h</td><td>{$food_production}/h</td></tr>
			<tr><td>Fuel</td><td>{$fleet->getFuel()}</td><td>{$nb_ships}/jump</td><td>{$fuel_production}/h</td></tr>
			<tr><td>Material</td><td>{$fleet->getMaterial()}</td><td>N/A</td><td>{$material_production}/5min</td></tr>
			<tr><td>Moral</td><td>{$fleet->getMoral()}</td><td>N/A</td><td>{$moral_production}/h</td></tr>
			<tr><td>Medicine</td><td>{$fleet->getMedicine()}</td><td>N/A</td><td>{$medicine_production}/h</td></tr>
			<tr><td>Staff</td><td>{$fleet->getQualifiedStaff()}</td><td>N/A</td><td>{$staff_production}/6h</td></tr>
			</tbody>
		</table>
	</div>
	<div class="clear"></div>
	<div id="production">
		{if $can_act && $can_produce}
		{if !empty($available_production)}
		<form action="produceship.php" method="post">
			<select name="shipid" id="select_prod">
			{foreach $available_production as $ship}
			<option value="{$ship->getId()}" title="{$ship->getPrice()}">{$i18n->getText($ship->getName())}</option>
			{/foreach}
			</select>
			<input type="number" name="nbShip" value="1" min="1" step="1"/>
			<input type="submit" value="{$lbl_build}"/>
		</form>
		{else}
		{$i18n->getText('msg.not.enough.material')}
		{/if}
		{/if}
	</div>
</div>

<script type="text/javascript">
sortable_table("tbl_fleet",[2,3,4]);
</script>
{if !empty($available_production)}
<script type="text/javascript">
sortSelect(document.getElementById("select_prod"));
</script>
{/if}
