<h1>{$lbl_received_messages}</h1>
<div class="datagrid">
	<table>
		<thead><tr><th>{$messages_th_message}</th><th>{$messages_th_date}</th></tr></thead>
		<tbody>
		{foreach $notifications as $notification}
		{$read = $notification->isRead()}
			<tr class="message-{if !$read}un{/if}read"><td>{$notification->getMessage($i18n)}</td><td>{$notification->getTime()}</td></tr>
		{foreachelse}
			<tr><td colspan=2>{$messages_no_notif}</td></tr>
		{/foreach}
		</tbody>
	</table>
</div>
<div class="datagrid">
	<table>
	<thead><tr><th>{$messages_th_sender}</th><th>{$messages_th_message}</th><th>{$messages_th_date}</th></tr></thead>
	<tbody>
	{foreach $messages as $message}
	{$read = $message->isRead()}
		<tr class="message-{if !$read}un{/if}read"><td>{$message->getSender()->getLogin()}</td><td><a href="index.php?page=message&amp;id={$message->getId()}">{$message->getTruncatedMessage({$length})}</a></td><td>{$message->getTime()}</td></tr>
	{foreachelse}
		<tr><td colspan=3>{$messages_no_message}</td></tr>
	{/foreach}
	</tbody>
	</table>
</div>
<div id="sendmessage">
<form action="sendmessage.php" method="post">
	<fieldset>
	<legend>{$messages_send}</legend>
	<dl>
		<dt>{$messages_recipient}</dt>
		<dd><input type="text" name="recipient" id="recipient"/></dd>
	</dl>
	<dl>
		<dt>{$messages_th_message}</dt>
		<dd><textarea name="message" id="message"></textarea></dd>
	</dl>
	<input class="action-button" type="submit" value="Envoyer"/>
	</fieldset>
</form>
</div>
