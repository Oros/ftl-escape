<h1>{$lbl_account}</h1>
<div>
	{$lbl_difficulty} : {$difficulty}
<form action="modifyaccount.php" method="post">
<dl>
	<dt>{$lbl_login}</dt>
	<dd>{$login}</dd>
	<dt>{$lbl_email}</dt>
	<dd><input type="email" name="email" value="{$email}" /></dd>
	<dt>{$lbl_current_password}</dt>
	<dd><input type="password" name="cpassword" /></dd>
	<dt>{$lbl_password}</dt>
	<dd><input type="password" name="npassword" /></dd>
	<dt>{$lbl_password_retype}</dt>
	<dd><input type="password" name="rpassword" /></dd>
</dl>
<input type="submit" value="{$lbl_save}" />
</form>
</div>

<h1>{$lbl_delete}</h1>
{if $account_in_deletion}
{$lbl_account_in_deletion} : {$deletion_date}<br />
<form action="canceldeletion.php" method="post">
<input type="submit" value="{$lbl_cancel}" />
</form>
{else}
<form action="deleteaccount.php" method="post">
<input type="checkbox" name="delcheck1" />
<input type="checkbox" name="delcheck2" />
<input type="checkbox" name="delcheck3" />
<input type="submit" value="{$lbl_delete}" />
</form>
{/if}
