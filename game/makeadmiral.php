<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../builder.php');
require_once(__DIR__.'/../lib/i18n.php');

$username = $_SESSION['username'];
$player = $entityManager->getRepository('Player')->findOneByLogin($username);

$i18n = new I18n();
$i18n->autoSetLang();

$id = $_GET['id'];

if (is_numeric($id))
{
	$fleet = $player->getFleet();
	$ship = $fleet->getShip($_GET['id']);
	if (!is_null($ship))
	{
		$fleet->setAdmiralShip($ship);
		$entityManager->flush();
	}
}
header('Location: index.php?page=fleet');
