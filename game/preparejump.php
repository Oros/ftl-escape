<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../lib/i18n.php');

$username = $_SESSION['username'];
$player = $entityManager->getRepository('Player')->findOneByLogin($username);

$i18n = new I18n();
$i18n->autoSetLang();

$fleet = $player->getFleet();

$sector = $player->getSector();

$ennemies = $sector->getEnnemies();

if (count($ennemies) == 0)
{
	$fleet->setJumpStatus(JUMP_STATUS_SAFE);
	Tools::setFlashMsg($i18n->getText('msg.jump.prepared'));
}

$entityManager->flush();
header('Location: index.php');
