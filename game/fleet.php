<?php
$fleet = $player->getFleet();
$sector = $player->getSector();
$canAct = count($sector->getEnnemies()) == 0;

$smarty->assign('can_act',$canAct);
$smarty->assign('fleetname',$fleet->getName());
$smarty->assign('fleet',$fleet);

$ships = $fleet->getShips();

$smarty->assign('ships',$ships);
$smarty->assign('nb_ships',count($ships));

$survivors = 0;
$foodproduction = 0;
$fuelproduction = 0;
$materialproduction = 0;
$moralproduction = 0;
$medicineproduction = 0;
$staffproduction = 0;
$canRepair=false;
$canScout=false;
$canMine=false;
$canProduce=false;
$defense=0;

foreach ($ships as $ship)
{
	$level = $ship->getLevel();
	$survivors += $ship->getPassengers() + $ship->getStaff();
	$foodproduction += $ship->getType()->getFoodProduction($level);
	$fuelproduction += $ship->getType()->getFuelProduction($level);
	$materialproduction += $ship->getType()->getMaterialProduction($level);
	$moralproduction += $ship->getType()->getMoralProduction();
	$medicineproduction += $ship->getType()->getMedicineProduction($level);
	$staffproduction += $ship->getType()->getQualifiedStaffPerCycle($level);
	$canRepair = $canRepair || $ship->getType()->canRepair();
	$canScout = $canScout || $ship->getType()->canScout();
	$canMine = $canMine || $ship->getType()->canMine();
	$canProduce = $canProduce || $ship->getType()->canProduceShips();
	$defense += $ship->getType()->getDefense($level);
}

$smarty->assign('survivors',$survivors);
$smarty->assign('food_production',round($foodproduction));
$smarty->assign('food_conso',round($survivors/10000));
$smarty->assign('fuel_production',round($fuelproduction));
$smarty->assign('material_production',round($materialproduction));
$smarty->assign('moral_production',round($moralproduction));
$smarty->assign('medicine_production',round($medicineproduction));
$smarty->assign('staff_production',round($staffproduction));
$smarty->assign('combined_attack',$fleet->getCombinedAttack());
$smarty->assign('combined_defense',$defense);
$smarty->assign('can_repair',$canRepair);
$smarty->assign('can_scout',$canScout);
$smarty->assign('can_mine',$canMine);
$smarty->assign('can_produce',$canProduce);
$smarty->assign('has_admiral_ship',!is_null($fleet->getAdmiralShip()));
$smarty->assign('admiral_ship',$fleet->getAdmiralShip());
$smarty->assign('admiral_defense_bonus',ADMIRAL_SHIP_DEFENSE_BONUS);
$smarty->assign('nb_ships',count($ships));
$smarty->assign('ship_level_multiplier',SHIP_LEVEL_MULTIPLIER);
$smarty->assign('upgrade_ratio',SHIP_UPGRADE_PRICE);
$smarty->assign('max_upgrade_level',SHIP_UPGRADE_MAX_LEVEL);

if ($canAct && $canProduce)
{
	$qb = $entityManager->createQueryBuilder();
	$qb->select('s')
		->from('ShipType','s')
		->where('s.price <= ?1')
		->setParameter(1,$fleet->getMaterial());
	$query = $qb->getQuery();
	$buildableShips = $query->getResult();
	$smarty->assign('available_production',$buildableShips);
}

// i18n

$smarty->assign('i18n',$i18n);
if (count($ships) > 1)
{
	$smarty->assign('w_ships',$i18n->getText('word.ships'));
}
else
{
	$smarty->assign('w_ships',$i18n->getText('word.ship'));
}
$smarty->assign('lbl_repair',$i18n->getText('lbl.repair'));
$smarty->assign('lbl_ship_name',$i18n->getText('lbl.ship.name'));
$smarty->assign('lbl_ship_type_name',$i18n->getText('lbl.ship.type.name'));
$smarty->assign('lbl_ship_hp',$i18n->getText('lbl.ship.hp'));
$smarty->assign('lbl_ship_attack',$i18n->getText('lbl.ship.attack'));
$smarty->assign('lbl_ship_defense',$i18n->getText('lbl.ship.defense'));
$smarty->assign('lbl_ship_passengers',$i18n->getText('lbl.ship.passengers'));
$smarty->assign('lbl_ship_staff',$i18n->getText('lbl.ship.staff'));
$smarty->assign('lbl_ship_fuel_production',$i18n->getText('lbl.ship.fuel.production'));
$smarty->assign('lbl_ship_food_production',$i18n->getText('lbl.ship.food.production'));
$smarty->assign('lbl_ship_medicine_production',$i18n->getText('lbl.ship.medicine.production'));
$smarty->assign('lbl_ship_moral_production',$i18n->getText('lbl.ship.moral.production'));
$smarty->assign('lbl_ship_material_production',$i18n->getText('lbl.ship.material.production'));
$smarty->assign('lbl_power_repair',$i18n->getText('lbl.power.repair'));
$smarty->assign('lbl_power_mine',$i18n->getText('lbl.power.mine'));
$smarty->assign('lbl_power_scout',$i18n->getText('lbl.power.scout'));
$smarty->assign('lbl_power_produce',$i18n->getText('lbl.power.produce'));
$smarty->assign('lbl_transfer',$i18n->getText('lbl.transfer'));
$smarty->assign('lbl_build',$i18n->getText('lbl.build'));
$smarty->assign('fleet_th_stock',$i18n->getText('fleet.th.stock'));
$smarty->assign('fleet_th_conso',$i18n->getText('fleet.th.conso'));
$smarty->assign('fleet_th_prod',$i18n->getText('fleet.th.prod'));
$smarty->assign('msg_not_enough_staff',$i18n->getText('msg.not.enough.staff'));
