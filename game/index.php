<?php
include __DIR__.'/../lib/session.inc.php';
check_login();
date_default_timezone_set('UTC');
require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../lib/i18n.php');
require_once(__DIR__.'/../helper.php');

$smarty = new Smarty();

$smarty->setTemplateDir('tpl');
$smarty->setCompileDir('tmp');
$smarty->setConfigDir('conf');
$smarty->setCacheDir('cache');

$smarty->assign('username',$_SESSION['username']);
/*$smarty->caching = 1;
$smarty->cache_lifetime = 300;
$smarty->compile_check = true;*/
$smarty->debugging = false;

$i18n = new I18n();
$i18n->autoSetLang();

$username = $_SESSION['username'];
$player = $entityManager->getRepository('Player')->findOneByLogin($username);
if (is_null($player))
{
	echo 'Account does not exist';
	exit;
}

if ($player->getFleet()->isEmpty() && !$player->isGameOver())
{
	Tools::gameOver($player);
}

$page = null;
if (array_key_exists('page',$_GET))
{
	$page = $_GET['page'];
}
$template_name = "";
$title = 'Fleet Command';

$qb = $entityManager->createQueryBuilder();
$qb->select('m')
	->from('Message','m')
	->where('m.recipient = :player and m.read=false')
	->setParameter('player',$player);
$query = $qb->getQuery();
$nbMessagesUnread = count($query->getResult());
$smarty->assign('nb_messages',$nbMessagesUnread);

$smarty->assign('account_deletion',!is_null($player->getDeletionDate()));

$smarty->assign('under_attack',Helper::underAttack($player));

$sector = $player->getSector();
$alertSector = $sector->getWrecks() > 0;
$smarty->assign('alert_sector',$alertSector);

if ($player->isGameOver())
{
	$page = 'gameover';
}

if ($page === 'gameover')
{
	$template_name='gameover';
}
elseif ($page === 'fleet')
{
	$template_name='fleet';
	include('fleet.php');
}
elseif ($page === 'sector')
{
	$template_name='sector';
	include('sector.php');
}
elseif ($page === 'encyclopedia')
{
	$template_name='encyclopedia';
	include('encyclopedia.php');
}
elseif ($page === 'history')
{
	$template_name='history';
	include('history.php');
}
elseif ($page === 'editfleetname')
{
	$template_name='editfleetname';
	include('editfleetname.php');
}
elseif ($page === 'editshipname')
{
	$template_name='editshipname';
	include('editshipname.php');
}
elseif ($page === 'messages')
{
	$template_name='messages';
	include('messages.php');
}
elseif ($page === 'message')
{
	$template_name='message';
	include('message.php');
}
elseif ($page === 'politics')
{
	$template_name='politics';
	include('politics.php');
}
elseif ($page === 'account')
{
	$template_name='account';
	include('account.php');
}
elseif ($page === 'transfer')
{
	$template_name='transfer';
	include('transfer.php');
}
elseif ($page === 'galaxy')
{
	$template_name='galaxy';
	include('galaxy.php');
}
else
{
	$template_name = "dashboard";
	include('dashboard.php');
}

$flashmsg = Tools::getFlashMsg();
$smarty->assign('flashmsg',$flashmsg);
$smarty->assign('hasflashmsg',$flashmsg != '');

$smarty->assign('title',$title);

$smarty->assign('template_name',$template_name);

// i18n

$smarty->assign('menu_dashboard',$i18n->getText('lbl.menu.dashboard'));
$smarty->assign('menu_messages',$i18n->getText('lbl.menu.messages'));
$smarty->assign('menu_encyclopedia',$i18n->getText('lbl.menu.encyclopedia'));
$smarty->assign('menu_signout',$i18n->getText('lbl.menu.signout'));
$smarty->assign('menu_sector',$i18n->getText('lbl.menu.sector'));
$smarty->assign('menu_fleet',$i18n->getText('lbl.menu.fleet'));
$smarty->assign('menu_history',$i18n->getText('lbl.menu.history'));
$smarty->assign('menu_event',$i18n->getText('lbl.menu.event'));
$smarty->assign('menu_politics',$i18n->getText('lbl.menu.politics'));
$smarty->assign('menu_account',$i18n->getText('lbl.menu.account'));
$smarty->assign('menu_galaxy',$i18n->getText('lbl.menu.galaxy'));
$smarty->assign('version',VERSION);
$smarty->display('index.tpl');
