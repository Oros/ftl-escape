<?php

$sector = $player->getSector();
$fleet = $player->getFleet();

$canScout = Helper::canScout($fleet);

$smarty->assign('materials',$sector->getMaterial());
$smarty->assign('wrecks',$sector->getWrecks());
$smarty->assign('can_scout',$canScout);
$smarty->assign('can_act',count($sector->getEnnemies())==0);

// i18n

$smarty->assign('lbl_materials',$i18n->getText('lbl.materials'));
$smarty->assign('lbl_wrecks',$i18n->getText('lbl.wrecks'));
$smarty->assign('lbl_scout',$i18n->getText('lbl.scout.wreck'));
$smarty->assign('lbl_menu_sector',$i18n->getText('lbl.menu.sector'));
