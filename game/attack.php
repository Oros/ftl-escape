<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../lib/i18n.php');

$username = $_SESSION['username'];
$player = $entityManager->getRepository('Player')->findOneByLogin($username);

$i18n = new I18n();
$i18n->autoSetLang();

$fleet = $player->getFleet();
$sector = $player->getSector();

// the fleet attacks the ennemies

if ($fleet->isEmpty())
{
	Tools::gameOver($player);
	header('Location: index.php');
}

if (is_numeric($_POST['ennemy']))
{
	$targetedEnnemy = $_POST['ennemy'];
	if ($targetedEnnemy == -1)
	{
		$ennemies = $sector->getEnnemies();
		$nbennemies = count($ennemies);
		$combinedattack = $fleet->getCombinedAttack();
		$averageattack = round(($combinedattack/$nbennemies),0);
		foreach ($ennemies as $ennemy)
		{
			$ennemy->takeDamage($averageattack);
			if ($ennemy->getHP() <= 0)
			{
				Tools::setFlashMsg($i18n->getText('msg.destroyed.ennemy.ship',array($ennemy->getName(),$i18n->getText($ennemy->getType()->getName()))));
				$entityManager->remove($ennemy);
				$sector->addWrecks(1);
			}
		}
	}
	else
	{
		$ennemy = $entityManager->getRepository('EnnemyShip')->find($_POST['ennemy']);
		if ($ennemy->getSector()->getId() == $player->getSector()->getId())
		{
			$ennemy->takeDamage($fleet->getCombinedAttack());
			if ($ennemy->getHP() <= 0)
			{
				Tools::setFlashMsg($i18n->getText('msg.destroyed.ennemy.ship',array($ennemy->getName(),$i18n->getText($ennemy->getType()->getName()))));
				$entityManager->remove($ennemy);
				$sector->addWrecks(1);
			}
		}
	}
}
$entityManager->flush();
// ennemies counter-attack

$ennemies = $sector->getEnnemies();
$ships = $fleet->getShips();
$ids = array();
foreach ($ships as $ship)
{
	array_push($ids,$ship->getId());
}
$targetedShip = $fleet->getShip($ids[rand(0,count($ids)-1)]);
$attackMode = rand(0,3);

if ($attackMode == 0)
{
	// one against one
	foreach ($ennemies as $ennemy)
	{
		$targetedShip->takeDamage($ennemy->getType()->getAttack());
		if ($targetedShip->getHP() <= 0)
		{
			if (!is_null($fleet->getAdmiralShip()) && $targetedShip->getId() == $fleet->getAdmiralShip()->getId())
			{
				$fleet->decreaseMoral(ADMIRAL_SHIP_LOST_MORAL_MALUS);
			}
			Tools::setFlashMsg($i18n->getText('msg.destroyed.fleet.ship',array($targetedShip->getName(),$i18n->getText($targetedShip->getType()->getName()))));
			$historyItem = new HistoryItem($targetedShip,$ennemy);
			$entityManager->persist($historyItem);
			$entityManager->remove($targetedShip);
			$sector->addWrecks(1);
			$fleet->decreaseMoral(1);
		}
		elseif ($targetedShip->getHP() <= $targetedShip->getType()->getMaxHP() * HP_DEATH_LIMIT)
		{
			$deadPassengers = rand(0,$targetedShip->getPassengers());
			$deadStaff = rand(0,$targetedShip->getStaff());
			$targetedShip->setPassengers($targetedShip->getPassengers()-$deadPassengers);
			$targetedShip->setStaff($targetedShip->getStaff()-$deadStaff);
		}
		$targetedShip = $fleet->getShip($ids[rand(0,count($ids)-1)]);
	}
}
elseif ($attackMode == 1)
{
	// concentrated fire
	$damage = 0;
	foreach ($ennemies as $ennemy)
	{
		$damage += $ennemy->getType()->getAttack();
	}
	$targetedShip->takeDamage($damage);
	if ($targetedShip->getHP() <= 0)
	{
		if (!is_null($fleet->getAdmiralShip()) && $targetedShip->getId() == $fleet->getAdmiralShip()->getId())
		{
			$fleet->decreaseMoral(ADMIRAL_SHIP_LOST_MORAL_MALUS);
		}
		Tools::setFlashMsg($i18n->getText('msg.destroyed.fleet.ship',array($targetedShip->getName(),$i18n->getText($targetedShip->getType()->getName()))));
		$historyItem = new HistoryItem($targetedShip);
		$entityManager->persist($historyItem);
		$entityManager->remove($targetedShip);
		$sector->addWrecks(1);
		$fleet->decreaseMoral(1);
	}
	elseif ($targetedShip->getHP() <= $targetedShip->getType()->getMaxHP() * HP_DEATH_LIMIT)
	{
		$deadPassengers = rand(0,$targetedShip->getPassengers());
		$deadStaff = rand(0,$targetedShip->getStaff());
		$targetedShip->setPassengers($targetedShip->getPassengers()-$deadPassengers);
		$targetedShip->setStaff($targetedShip->getStaff()-$deadStaff);
	}
}
elseif ($attackMode == 2)
{
	// distributed fire
	$damage = 0;
	foreach ($ennemies as $ennemy)
	{
		$damage += $ennemy->getType()->getAttack();
	}
	$damage = round($damage / count($ennemies));
	$ships = $fleet->getShips();
	foreach ($ships as $targetedShip)
	{
		$targetedShip->takeDamage($damage);
		if ($targetedShip->getHP() <= 0)
		{
			if (!is_null($fleet->getAdmiralShip()) && $targetedShip->getId() == $fleet->getAdmiralShip()->getId())
			{
				$fleet->decreaseMoral(ADMIRAL_SHIP_LOST_MORAL_MALUS);
			}
			Tools::setFlashMsg($i18n->getText('msg.destroyed.fleet.ship',array($targetedShip->getName(),$i18n->getText($targetedShip->getType()->getName()))));
			$historyItem = new HistoryItem($targetedShip);
			$entityManager->persist($historyItem);
			$entityManager->remove($targetedShip);
			$sector->addWrecks(1);
			$fleet->decreaseMoral(1);
		}
		elseif ($targetedShip->getHP() <= $targetedShip->getType()->getMaxHP() * HP_DEATH_LIMIT)
		{
			$deadPassengers = rand(0,$targetedShip->getPassengers());
			$deadStaff = rand(0,$targetedShip->getStaff());
			$targetedShip->setPassengers($targetedShip->getPassengers()-$deadPassengers);
			$targetedShip->setStaff($targetedShip->getStaff()-$deadStaff);
		}
	}
}
elseif ($attackMode == 3)
{
	// nothing... well, let's say combat formation movement
}

if ($fleet->getJumpStatus() < MAX_JUMP_STATUS)
{
	if (count($fleet->getShips()) == 1 && $fleet->getJumpStatus()+1 >= MIN_JUMP_STATUS_FOR_JUMP)
	{
		// if there is only one ship left in fleet, sync with fleet is not necessary
		$fleet->setJumpStatus(MAX_JUMP_STATUS);
	}
	else
	{
		$fleet->setJumpStatus($fleet->getJumpStatus() + 1);
	}
}

header('Location:index.php');

$entityManager->flush();
