<?php

$smarty->assign('ships',$entityManager->getRepository('ShipType')->findAll());
$smarty->assign('ennemy_ships',$entityManager->getRepository('EnnemyShipType')->findAll());
$smarty->assign('i18n',$i18n);

$definitions = array();

array_push($definitions,array('icon'=>'admiral_ship','title'=>$i18n->getText('enc.admiral.ship'),'definition'=>$i18n->getText('enc.admiral.ship.definition')));
array_push($definitions,array('icon'=>'upgrade','title'=>$i18n->getText('enc.upgrade'),'definition'=>$i18n->getText('enc.upgrade.definition')));
array_push($definitions,array('icon'=>null,'title'=>$i18n->getText('enc.staff.formation'),'definition'=>$i18n->getText('enc.staff.formation.definition')));
array_push($definitions,array('icon'=>null,'title'=>$i18n->getText('enc.jump'),'definition'=>$i18n->getText('enc.jump.definition')));
array_push($definitions,array('icon'=>null,'title'=>$i18n->getText('enc.weaponry'),'definition'=>$i18n->getText('enc.weaponry.definition')));

$smarty->assign('definitions',$definitions);

$difficulties = array();

for ($i=1;$i<=MAX_DIFFICULTY;$i++)
{
	$difficulties[$i] = $i18n->getText('lbl.difficulty.'.$i);
}
$smarty->assign('difficulties',$difficulties);
$smarty->assign('enc_th_type', $i18n->getText('enc.th.type'));
$smarty->assign('enc_th_hp', $i18n->getText('enc.th.hp'));
$smarty->assign('enc_th_attack', $i18n->getText('enc.th.attack'));
$smarty->assign('enc_th_defense', $i18n->getText('enc.th.defense'));
$smarty->assign('enc_th_passengers', $i18n->getText('enc.th.passengers'));
$smarty->assign('enc_th_staff', $i18n->getText('enc.th.staff'));
$smarty->assign('enc_th_food', $i18n->getText('enc.th.food'));
$smarty->assign('enc_th_fuel', $i18n->getText('enc.th.fuel'));
$smarty->assign('enc_th_prod', $i18n->getText('enc.th.prod'));
$smarty->assign('enc_th_medicine', $i18n->getText('enc.th.medicine'));
$smarty->assign('enc_th_materials', $i18n->getText('enc.th.materials'));
$smarty->assign('enc_th_moral', $i18n->getText('enc.th.moral'));
$smarty->assign('enc_th_price', $i18n->getText('enc.th.price'));
$smarty->assign('enc_th_difficulty', $i18n->getText('enc.th.difficulty'));
$smarty->assign('enc_lbl_humans', $i18n->getText('enc.lbl.humans'));
$smarty->assign('enc_lbl_enemy', $i18n->getText('enc.lbl.enemy'));
$smarty->assign('lbl_power_repair', $i18n->getText('lbl.power.repair'));
$smarty->assign('lbl_power_mine', $i18n->getText('lbl.power.mine'));
$smarty->assign('lbl_power_scout', $i18n->getText('lbl.power.scout'));
$smarty->assign('lbl_power_produce', $i18n->getText('lbl.power.produce'));
$smarty->assign('enc_lbl_history',$i18n->getText('lbl.history'));
$smarty->assign('enc_lbl_concepts',$i18n->getText('lbl.concepts'));
$smarty->assign('enc_txt_history',$i18n->getText('txt.history'));
