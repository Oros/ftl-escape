<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once __DIR__.'/../bootstrap.php';
require_once __DIR__.'/../const.php';
require_once __DIR__.'/../builder.php';

$username = $_SESSION['username'];
$player = $entityManager->getRepository('Player')->findOneByLogin($username);

if ($player->isGameOver())
{
	$fleet = $player->getFleet();
	Builder::reinitFleetStocks($fleet);
	Builder::populateFleet($fleet);
	$fleet->setJumpStatus(JUMP_STATUS_IDLE);
	$sector = $player->getSector();
	$ennemies = $sector->getEnnemies();
	foreach($ennemies as $ennemy)
	{
		$entityManager->remove($ennemy);
	}
	$entityManager->remove($sector);
	$entityManager->flush();
	
	$sector = Builder::buildSector($player,true);

	$entityManager->persist($sector);
	
	$qb = $entityManager->createQueryBuilder();

	$qb->select('m')
	->from ('Message', 'm')
	->where ('m.recipient = :player and m.sender is null')
	->setParameter('player',$player);

	$query = $qb->getQuery();
	$notifications = $query->getResult();
	foreach ($notifications as $notification)
	{
		$entityManager->remove($notification);
	}

	$player->restart();
	
	$entityManager->flush();
}
else
{
	echo "It's not finished for you yet.";
}

header('Location: index.php');
