<?php

include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../lib/i18n.php');

$username = $_SESSION['username'];
$player = $entityManager->getRepository('Player')->findOneByLogin($username);

$i18n = new I18n();
$i18n->autoSetLang();

$shipid = $_POST['shipid'];

if (!is_numeric($shipid))
{
	echo "Nope.";
	exit;
}

$nbShip = $_POST['nbShip'];

if (!is_numeric($nbShip) || $nbShip < 1)
{
        echo "Nope.";
        exit;
}

$fleet = $player->getFleet();

if (Helper::canProduce($fleet))
{

	$shipType = $entityManager->getRepository('ShipType')->find($shipid);

	if ($shipType->getPrice() * $nbShip > $fleet->getMaterial())
	{
		Tools::setFlashMsg($i18n->getText('msg.not.enough.material'));
		header('Location: index.php?page=fleet');
		exit;
	}
	while($nbShip > 0)
	{
		$newShip = new Ship($fleet,$shipType);
		$lines = file(__DIR__.'/../'.DICO_REP.$shipType->getNameDico().'.txt');
		$name = $lines[array_rand($lines)];
		$newShip->setName($name);
		$newShip->setHP($shipType->getMaxHP());
		$newShip->setPassengers(0);
		$newShip->setStaff(0);
		$entityManager->persist($newShip);
		$fleet->decreaseMaterial($shipType->getPrice());
		$nbShip--;
	}
	Tools::setFlashMsg($i18n->getText('msg.new.ship.created',array($i18n->getText($shipType->getName()))));
}
else
{
	Tools::setFlashMsg($i18n->getText('msg.cannot.produce'));
}
header('Location: index.php?page=fleet');
$entityManager->flush();
