<?php

$smarty->assign('players',$entityManager->getRepository('Player')->findAll());

// i18n
$smarty->assign('lbl_player',$i18n->getText('lbl.commander'));
$smarty->assign('lbl_nb_ships',$i18n->getText('lbl.nb.ships'));
$smarty->assign('lbl_nb_survivors',$i18n->getText('lbl.nb.survivors'));
$smarty->assign('lbl_galaxy',$i18n->getText('lbl.menu.galaxy'));
