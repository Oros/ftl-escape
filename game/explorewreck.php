<?php

include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../builder.php');
require_once(__DIR__.'/../lib/i18n.php');

$username = $_SESSION['username'];
$player = $entityManager->getRepository('Player')->findOneByLogin($username);

$i18n = new I18n();
$i18n->autoSetLang();

$fleet = $player->getFleet();
$sector = $player->getSector();

if (count($sector->getEnnemies()) == 0 && Helper::canScout($fleet) && $sector->getWrecks() > 0)
{
	$dice = rand(1,100);
	if ($dice <= CHANCE_OF_AMBUSH_PER_WRECK)
	{
		Builder::populateSector($sector,rand(1,MAX_ENNEMIES_PER_AMBUSH));
		Tools::setFlashMsg($i18n->getText('msg.ambush'));
	}
	else
	{
		$food = rand(0,MAX_FOOD_PER_WRECK);
		$fuel = rand(0,MAX_FUEL_PER_WRECK);
		$material = rand(0,MAX_MATERIAL_PER_WRECK);
		$medicine = rand(0,MAX_MEDICINE_PER_WRECK);
		Tools::setFlashMsg($i18n->getText('msg.found.valuables.in.wreck',array($food,$fuel,$material,$medicine)));
		$fleet->setFood($fleet->getFood() + $food);
		$fleet->increaseFuel($fuel);
		$fleet->increaseMaterial($material);
		$fleet->increaseMedicine($medicine);
		$sector->removeWrecks(1);
	}
}
else
{
	Tools::setFlashMsg($i18n->getText('msg.not.allowed'));
}

$entityManager->flush();
header('Location: index.php?page=sector');
