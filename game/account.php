<?php

$smarty->assign('login',$player->getLogin());
$smarty->assign('email',$player->getEmail());
$deletiondate = $player->getDeletionDate();
$smarty->assign('account_in_deletion',!is_null($deletiondate));
if (!is_null($deletiondate))
{
	$smarty->assign('deletion_timestamp',($deletiondate + DELETION_PERIOD));
	$smarty->assign('deletion_date',date('d/m/Y H:i e',($deletiondate + DELETION_PERIOD)));
}

$smarty->assign('difficulty',$i18n->getText('lbl.difficulty.'.Helper::calculateDifficulty($player)));

// I18n
$smarty->assign('lbl_account',$i18n->getText('lbl.account'));
$smarty->assign('lbl_login',$i18n->getText('lbl.login'));
$smarty->assign('lbl_email',$i18n->getText('lbl.email'));
$smarty->assign('lbl_password',$i18n->getText('lbl.password'));
$smarty->assign('lbl_password_retype',$i18n->getText('lbl.password.again'));
$smarty->assign('lbl_save',$i18n->getText('lbl.save'));
$smarty->assign('lbl_delete',$i18n->getText('lbl.delete.account'));
$smarty->assign('lbl_account_in_deletion',$i18n->getText('lbl.account.in.deletion'));
$smarty->assign('lbl_cancel',$i18n->getText('lbl.cancel'));
$smarty->assign('lbl_current_password',$i18n->getText('lbl.current.password'));
$smarty->assign('lbl_difficulty',$i18n->getText('lbl.difficulty'));
