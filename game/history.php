<?php

$fleet = $player->getFleet();
$history = $fleet->getHistory();

$smarty->assign('i18n',$i18n);
$smarty->assign('history',$history);
$smarty->assign('lbl_lost_ships',$i18n->getText('lbl.lost_ships'));
$smarty->assign('hist_th_name',$i18n->getText('hist.th.name'));
$smarty->assign('hist_th_passengers',$i18n->getText('hist.th.passengers'));
$smarty->assign('hist_th_killedby',$i18n->getText('hist.th.killedby'));
$smarty->assign('hist_th_date',$i18n->getText('hist.th.date'));

