<?php

require_once(__DIR__.'/bootstrap.php');
require_once __DIR__.'/const.php';
require_once(__DIR__.'/lib/i18n.php');
require_once(__DIR__.'/tools.php');
require_once(__DIR__.'/helper.php');

class Builder {
	static function buildFleet($player)
	{
		global $entityManager;
		$fleet = new Fleet($player);
		$fleet->setName('Default Fleet Name');
		
		Builder::populateFleet($fleet,true);
		
		return $fleet;
	}
	
	static function reinitFleetStocks($fleet)
	{
		$fleet->setFood(INIT_FOOD);
		$fleet->setFuel(INIT_FUEL);
		$fleet->setMaterial(INIT_MATERIAL);
		$fleet->setMedicine(INIT_MEDICINE);
		$fleet->setMoral(INIT_MORAL);
	}
	
	static function populateFleet(&$fleet,$first=false)
	{
		global $entityManager;
		$battlecruiserType = $entityManager->getRepository('ShipType')->find(BATTLECRUISER_ID);

		$battlecruiser = new Ship($fleet,$battlecruiserType);
		$battlecruiser->setName('Default Battlecruiser Name');
		$battlecruiser->setHP($battlecruiserType->getMaxHP());
		$battlecruiser->setPassengers($battlecruiserType->getMaxPassengers());
		$battlecruiser->setStaff($battlecruiserType->getQualifiedStaff());
		$entityManager->persist($battlecruiser);
		
		if ($first)
		{
			$fleet->setAdmiralShip($battlecruiser);
		}

		$nbShip = rand(MIN_START_NB_SHIP,MAX_START_NB_SHIP);
		// hugly hack : does not work if you delete a ShipType
		// @TODO : find a better way to randomize ship types
		$nbAvailableShips = count($entityManager->getRepository('ShipType')->findAll());

		$canMine=false;
		$canScout=false;
		$canRepair=false;
		$canProduce=false;

		for ($num=0;$num<$nbShip;$num++)
		{
			$randType = $entityManager->getRepository('ShipType')->find(rand(RANDOM_SHIPS_START_INDEX,$nbAvailableShips));
			$canMine = $canMine || $randType->canMine();
			$canScout = $canScout || $randType->canScout();
			$canRepair = $canRepair || $randType->canRepair();
			$canProduce = $canProduce || $randType->canProduceShips();
			Builder::buildShip($fleet,$randType,rand(0,$randType->getMaxPassengers()),$randType->getQualifiedStaff());
		}
		if ($first)
		{
			$fleet->setMaterial(INIT_MATERIAL);
			$fleet->setFood(INIT_FOOD);
			$fleet->setFuel(INIT_FUEL);
			$fleet->setMoral(INIT_MORAL);
			$fleet->setMedicine(INIT_MEDICINE);
			if (!$canMine)
			{
				$ships = $entityManager->getRepository('ShipType')->findBy(array('mine'=>true));
				$nb = count($ships);
				$select = rand(0,$nb-1);
				$shipType = $ships[$select];
				Builder::buildShip($fleet,$shipType,rand(0,$shipType->getMaxPassengers()),$shipType->getQualifiedStaff());
			}
			if (!$canScout)
			{
				$ships = $entityManager->getRepository('ShipType')->findBy(array('scout'=>true));
				$nb = count($ships);
				$select = rand(0,$nb-1);
				$shipType = $ships[$select];
				Builder::buildShip($fleet,$shipType,rand(0,$shipType->getMaxPassengers()),$shipType->getQualifiedStaff());
			}
			if (!$canRepair)
			{
				$ships = $entityManager->getRepository('ShipType')->findBy(array('repair'=>true));
				$nb = count($ships);
				$select = rand(0,$nb-1);
				$shipType = $ships[$select];
				Builder::buildShip($fleet,$shipType,rand(0,$shipType->getMaxPassengers()),$shipType->getQualifiedStaff());
			}
			if (!$canProduce)
			{
				$ships = $entityManager->getRepository('ShipType')->findBy(array('produceShips'=>true));
				$nb = count($ships);
				$select = rand(0,$nb-1);
				$shipType = $ships[$select];
				Builder::buildShip($fleet,$shipType,rand(0,$shipType->getMaxPassengers()),$shipType->getQualifiedStaff());
			}
		}
	}
	
	static function buildShip($fleet,$type,$passengers=0,$staff=0)
	{
		global $entityManager;
		$newShip = new Ship($fleet,$type);
		$lines = file(__DIR__.'/'.DICO_REP.$type->getNameDico().'.txt');
		$name = $lines[array_rand($lines)];
		$newShip->setName($name);
		$newShip->setHP($type->getMaxHP());
		$newShip->setPassengers($passengers);
		$newShip->setStaff($staff);
		$entityManager->persist($newShip);
	}
	
	static function buildSector($player,$firstsector=false)
	{
		global $entityManager;
		$i18n = new I18n();
		$i18n->autoSetLang();
		$sector = new Sector($player);
		$dice = 0;
		if (!$firstsector)
		{
			$dice = rand(1,100);
		}
		$hasEnnemies = $firstsector || $dice <= 20;
		if ($hasEnnemies)
		{
			$nbEnnemies=0;
			if ($firstsector)
			{
				$nbEnnemies = rand(1,MAX_START_NB_ENNEMIES);
			}
			else
			{
				$nbEnnemies = rand(1,MAX_ENNEMIES_PER_SECTOR);
			}
			Builder::populateSector($sector,$nbEnnemies,$firstsector);
			if (!$firstsector)
			{
				Tools::setFlashMsg($i18n->getText('msg.ennemy.detected'));
			}
		}
		else
		{
			Tools::setFlashMsg($i18n->getText('msg.no.ennemy.detected'));
		}
		
		if (!$firstsector)
		{
			$material = rand(SECTOR_MIN_MATERIAL,SECTOR_MAX_MATERIAL);
			$sector->setMaterial($material);
		}
		
		// generate wrecks
		$dice = rand(1,100);
		
		if ($dice <= CHANCE_OF_WRECK_IN_SECTOR)
		{
			$nbwreck = rand(1,MAX_WRECKS_PER_SECTOR);
			$sector->addWrecks($nbwreck);
		}
		return $sector;
	}
	
	static function populateSector(&$sector,$nbEnnemies,$firstsector=false)
	{
		global $entityManager;
		$player = $sector->getPlayer();
		// hugly hack : does not work if you delete a ShipType
		// @TODO : find a better way to randomize ship types
		$nbAvailableShips = count($entityManager->getRepository('EnnemyShipType')->findAll());

		for ($num=0;$num<$nbEnnemies;$num++)
		{
			$randType = null;
			$difficulty = MAX_DIFFICULTY + 1;
			$maxdifficulty = MAX_DIFFICULTY;
			if ($firstsector)
			{
				$difficulty = MAX_START_DIFFICULTY + 1;
				$maxdifficulty = MAX_START_DIFFICULTY;
			}
			else
			{
				$maxdifficulty = Helper::calculateDifficulty($player);
			}
			while ($difficulty > $maxdifficulty)
			{
				$randType = $entityManager->getRepository('EnnemyShipType')->find(rand(1,$nbAvailableShips));
				$difficulty = $randType->getDifficulty();
			}
			$ennemyShip = new EnnemyShip($sector,$randType,'Bandit '.$num);
			$ennemyShip->setHP($randType->getMaxHP());
			$entityManager->persist($ennemyShip);
		}
	}
}
