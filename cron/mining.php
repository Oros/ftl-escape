<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');

$players = $entityManager->getRepository('Player')->findAll();

foreach ($players as $player)
{
	$fleet = $player->getFleet();
	$sector = $player->getSector();
	if (!is_null($sector))
	{
		$availableMaterial = $sector->getMaterial();
		if ( $availableMaterial > 0)
		{
			$miningpower = 0;
			$ships = $fleet->getShips();
			foreach ($ships as $ship)
			{
				$miningpower += $ship->getType()->getMaterialProduction($ship->getLevel());
			}
			if ($availableMaterial >= $miningpower)
			{
				$sector->setMaterial($availableMaterial - $miningpower);
				$fleet->increaseMaterial($miningpower);
			}
			else
			{
				$sector->setMaterial(0); 
				$fleet->increaseMaterial($availableMaterial);
			}
		}
	}
}

$entityManager->flush();
