<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');

$players = $entityManager->getRepository('Player')->findAll();

foreach ($players as $player)
{
	$fleet = $player->getFleet();
	$ships = $fleet->getShips();
	$fuelproduction=0;
	foreach ($ships as $ship)
	{
		$fuelproduction += $ship->getType()->getFuelProduction($ship->getLevel());
	}
	$fleet->increaseFuel($fuelproduction);
}

$entityManager->flush();
