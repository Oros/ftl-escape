<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');

$players = $entityManager->getRepository('Player')->findAll();

foreach ($players as $player)
{
	$sector = $player->getSector();
	if (!is_null($sector))
	{
		$ennemies = $sector->getEnnemies();
		if (count($ennemies) == 0)
		{
			$dice = rand(1,100);
			if ($dice <= CHANCE_OF_EVENT)
			{
				$fleet = $player->getFleet();
				// hugly hack : does not work if you delete a ShipType
				// @TODO : find a better way to randomize ship types
				$nbAvailableShips = count($entityManager->getRepository('ShipType')->findAll());
				$randType = $entityManager->getRepository('ShipType')->find(rand(RANDOM_SHIPS_START_INDEX,$nbAvailableShips));
				$newShip = new Ship($fleet,$randType);
				$lines = file($docroot.DICO_REP.$randType->getNameDico().'.txt');
				$name = $lines[array_rand($lines)];
				$newShip->setName($name);
				$newShip->setHP($randType->getMaxHP());
				$newShip->setPassengers(rand(0,$randType->getMaxPassengers()));
				$newShip->setStaff($randType->getQualifiedStaff());
				$entityManager->persist($newShip);
				$message = new Message(null,$player,'msg.encounter.new.ship',true);
				$entityManager->persist($message);
			}
		}
	}
}

$entityManager->flush();
