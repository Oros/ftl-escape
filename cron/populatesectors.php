<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');
require_once($docroot.'/helper.php');

$players = $entityManager->getRepository('Player')->findAll();

foreach ($players as $player)
{
	$sector = $player->getSector();
	if (!is_null($sector))
	{
		if (count($sector->getEnnemies()) == 0)
		{
			$dice = rand(1,100);
			if ($dice <= CHANCE_OF_ATTACK)
			{
				$nbEnnemies = rand(1,MAX_ENNEMIES_PER_SECTOR);
				// hugly hack : does not work if you delete a ShipType
				// @TODO : find a better way to randomize ship types
				$nbAvailableShips = count($entityManager->getRepository('EnnemyShipType')->findAll());
				$maxdifficulty = Helper::calculateDifficulty($player);
				$difficulty = $maxdifficulty + 1;
				$randType = null;
				for ($num=0;$num<$nbEnnemies;$num++)
				{
					$difficulty = $maxdifficulty + 1;
					while ($difficulty > $maxdifficulty)
					{
						$randType = $entityManager->getRepository('EnnemyShipType')->find(rand(1,$nbAvailableShips));
						$difficulty = $randType->getDifficulty();
					}
					$ennemyShip = new EnnemyShip($sector,$randType,'Bandit '.$num);
					$ennemyShip->setHP($randType->getMaxHP());
					$entityManager->persist($ennemyShip);
				}
			}
		}
	}
}

$entityManager->flush();
