{* I am a Smarty comment, I don't exist in the compiled output  *}
<!DOCTYPE html>
<html>
<head>
	<title>{$title}</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="initial-scale=1.0, user-scalable=yes"/>
	<link rel="stylesheet" href="css/main.css" type="text/css"/>
</head>
<body>
	<div id="header">
	{include file='header.tpl'}
	</div>
	<div id="body-container">
		{if $hasMessage}
		<div id="flashmsg">
			{$msg}
		</div>
		{/if}
		{if $init}
		<div id="top">
			<div id="login-form" class="form">
			<form action="login.php" method="post">
				<fieldset>
					<legend>{$lbl_connect}</legend>
					<dl>
						<dd><input type="text" name="login" id="login" placeholder="{$lbl_login}"/></dd>
					</dl>
					<dl>
						<dd><input type="password" name="password" id="password" placeholder="{$lbl_password}"/></dd>
					</dl>
					<input class="action-button" type="submit" value="{$lbl_connect}"/>
				</fieldset>
			</form>
			</div>

			<div id="signup-form" class="form">
			<form action="signup.php" method="post">
				<fieldset>
					<legend>{$lbl_signup}</legend>
					<dl>
						<dd><input type="text" name="username" id="username" placeholder="{$lbl_login}*"/></dd>
					</dl>
					<dl>
						<dd><input type="password" name="spassword" id="spassword" placeholder="{$lbl_password}*"/></dd>
					</dl>
					<dl>
						<dd><input type="password" name="rspassword" id="rspassword" placeholder="{$lbl_password_again}*"/></dd>
					</dl>
					<dl>
						<dd><input type="email" name="email" id="email" placeholder="{$lbl_email}"/></dd>
					</dl>
					<input class="action-button" type="submit" value="{$lbl_signup}"/>
				</fieldset>
			</form>
			</div>
		</div>
	</div>
	<div id="synopsis">
	{$txt_synopsis}
	</div>
	{else}
	Jeu non-initialisé
	{/if}
	<div class="clear"></div>
	<br />
	<div id="footer">
	{include file='footer.tpl'}
	</div>
</body>
</html>
